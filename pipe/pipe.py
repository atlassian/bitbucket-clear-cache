import os
import requests

import yaml

from bitbucket_pipes_toolkit import Pipe


BITBUCKET_API_REPOSITORY_URL = 'https://api.bitbucket.org/2.0/repositories'


schema = {
    'BITBUCKET_USERNAME': {'required': False, 'type': 'string', 'excludes': 'BITBUCKET_ACCESS_TOKEN'},
    'BITBUCKET_APP_PASSWORD': {'required': False, 'type': 'string', 'excludes': 'BITBUCKET_ACCESS_TOKEN'},
    'BITBUCKET_ACCESS_TOKEN': {'required': False, 'type': 'string', 'excludes': ['BITBUCKET_USERNAME', 'BITBUCKET_APP_PASSWORD']},
    # NOTE: overridden global variable BITBUCKET_WORKSPACE will not be taken into account
    'WORKSPACE': {'required': False, 'type': 'string', 'default': os.getenv('BITBUCKET_WORKSPACE')},
    'REPO_SLUG': {'required': False, 'type': 'string', 'default': os.getenv('BITBUCKET_REPO_SLUG')},
    'CACHES_COUNT': {'required': False, 'type': 'integer', 'default': 1},
    'CACHES': {'required': False, 'type': 'list', 'default': []},
    'DEBUG': {'type': 'boolean', 'required': False, 'default': False}
}


class BitbucketClearCache(Pipe):
    def __init__(self, pipe_metadata=None, schema=None, env=None, check_for_newer_version=False):
        super().__init__(
            pipe_metadata=pipe_metadata,
            schema=schema,
            env=env,
            check_for_newer_version=check_for_newer_version
        )

        self.workspace = self.get_variable('WORKSPACE')
        self.repo_name = self.get_variable('REPO_SLUG')
        self.bitbucket_user = self.get_variable('BITBUCKET_USERNAME')
        self.bitbucket_password = self.get_variable('BITBUCKET_APP_PASSWORD')
        self.auth_token = self.get_variable('BITBUCKET_ACCESS_TOKEN')
        self.caches_to_clear = self.get_variable('CACHES')
        self.headers = {'Content-Type': 'application/json'}
        self.auth = self.resolve_auth()

    def run(self):
        self.log_info('Executing the pipe...')

        url = f'{BITBUCKET_API_REPOSITORY_URL}/{self.workspace}/{self.repo_name}/pipelines-config/caches/?page=1&pagelen=100'
        response = requests.get(url, auth=self.auth)

        if not response.ok:
            self.fail(f'Failed to retrieve caches: {response.status_code} {response.text} {response.request.url}')

        self.log_debug(response.request.headers)
        self.log_debug(response.content)

        response_json = response.json()

        if not self.caches_to_clear:
            self.clear_all_caches(cache_list_json=response_json["values"])
        else:
            self.clear_selected_caches(cache_list_json=response_json["values"], caches_to_clear=self.caches_to_clear)

        self.success('Finished clearing caches')

    def clear_all_caches(self, cache_list_json):
        self.success(f'Retrieved {len(cache_list_json)} caches')

        for cache in cache_list_json:
            self.log_debug(cache)
            self.clear_cache_by_uuid(cacheUuid=cache["uuid"], cache_name=cache["name"])

    def clear_selected_caches(self, cache_list_json, caches_to_clear):
        for cache in cache_list_json:
            self.log_debug(cache)
            if cache["name"] in caches_to_clear:
                self.clear_cache_by_uuid(cacheUuid=cache["uuid"], cache_name=cache["name"])

    def clear_cache_by_uuid(self, cacheUuid, cache_name):
        delete_url = f'{BITBUCKET_API_REPOSITORY_URL}/{self.workspace}/{self.repo_name}/pipelines-config/caches/{cacheUuid}'
        delete_response = requests.delete(delete_url, auth=self.auth)

        if delete_response.ok:
            self.success(f'Successfully cleared cache {cache_name}')
        else:
            self.fail(f'Failed to clear cache {cache_name}: {delete_response.text}')


if __name__ == '__main__':
    with open('/pipe.yml', 'r') as metadata_file:
        metadata = yaml.safe_load(metadata_file.read())

    pipe = BitbucketClearCache(pipe_metadata=metadata, schema=schema, check_for_newer_version=True)
    pipe.run()
