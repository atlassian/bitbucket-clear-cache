# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 3.6.2

- patch: Internal maintenance: Bump release pipe version. Add multi-platform build: linux/arm64 and linux/amd64.

## 3.6.1

- patch: Internal maintenance: Bump pipes versions in pipelines config file.
- patch: Internal maintenance: Update tests to authenticate only via bitbucket access token.

## 3.6.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 3.5.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 3.4.0

- minor: Update bitbucket-pipes-toolkit to fix vulnerabilities with certify and gitpython.
- patch: Internal maintenance: bump pipes versions in pipelines config file.

## 3.3.0

- minor: Add support for token based authentication.
- patch: Internal maintenance: update docker image and packages.

## 3.2.0

- minor: Add support for DEBUG variable.
- patch: Internal maintenance: Bump base docker image to python:3.9-slim.
- patch: Internal maintenance: Bump version of the bitbucket-pipe-release.
- patch: Internal maintenance: Update the communty link.

## 3.1.1

- patch: Update pipe.yml for latest pipelines format

## 3.1.0

- minor: change cache APIs to use public APIs

## 3.0.0

- major: Bugfix: remove BITBUCKET_OWNER_UUID as required variable. Add optional WORKSPACE variable. Change BITBUCKET_REPO_SLUG to REPO_SLUG.

## 2.0.5

- patch: Internal maintenance: Add gitignore secrets.

## 2.0.4

- patch: Internal maintenance: Upgrade dependency version bitbucket-pipes-toolkit.
- patch: Update the Readme with a new Atlassian Community link.

## 2.0.3

- patch: Internal maintenance: Add hadolint linter for Dockerfile

## 2.0.2

- patch: Add warning message about new version of the pipe available.

## 2.0.1

- patch: Fixed missing docker image name in the pipe metadata file

## 2.0.0

- major: Pipe name was changed from clear-cache to bitbucket-clear-cache

## 1.0.1

- patch: Updated the documentation

## 1.0.0

- major: Initial public release of the pipe

## 0.1.3

- patch: Updated pipes toolkit version to fix coloring of log info messages.

## 0.1.2

- patch: Updated the internal library version

## 0.1.1

- patch: Improve documentation and release process

## 0.1.0

- minor: Initial release
