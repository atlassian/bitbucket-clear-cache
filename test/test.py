import os
import time

import requests

from bitbucket_pipes_toolkit import TokenAuth
from bitbucket_pipes_toolkit.test import PipeTestCase
from bitbucket_pipes_toolkit import ArrayVariable


BITBUCKET_REPO_SLUG = 'test-trigger-build'
BITBUCKET_API_BASE_URL = 'https://api.bitbucket.org/2.0'
BITBUCKET_API_REPOSITORIES_URL = f'{BITBUCKET_API_BASE_URL}/repositories'


class ClearCacheTestCase(PipeTestCase):

    maxDiff = None

    def setUp(self):
        super().setUp()

        auth = TokenAuth(os.getenv("BITBUCKET_ACCESS_TOKEN"))
        headers = {'Content-Type': 'application/json'}
        data = {
            "target": {
                "ref_type": 'branch',
                "type": "pipeline_ref_target",
                "ref_name": 'master'
            }
        }

        response = requests.post(
            f'{BITBUCKET_API_REPOSITORIES_URL}/bbcitest/{BITBUCKET_REPO_SLUG}/pipelines/',
            headers=headers,
            auth=auth,
            json=data
        )
        response.raise_for_status()

        response_data = response.json()
        build_number = response_data['build_number']

        for i in range(300):
            response = requests.get(
                f'{BITBUCKET_API_REPOSITORIES_URL}/bbcitest/{BITBUCKET_REPO_SLUG}/pipelines/{build_number}', auth=auth)

            build_data = response.json()
            build_state = build_data['state']['name']

            if build_state == 'COMPLETED':
                break

            time.sleep(1)

    def test_default_success(self):
        # default current workspace will be taken
        result = self.run_container(environment={
            "BITBUCKET_ACCESS_TOKEN": os.getenv("BITBUCKET_ACCESS_TOKEN_CURRENT")
        })
        self.assertIn('Finished clearing caches', result)

    def test_clear_selected_caches(self):
        # WORKSPACE from repository variables
        result = self.run_container(environment={
            "BITBUCKET_ACCESS_TOKEN": os.getenv("BITBUCKET_ACCESS_TOKEN"),
            "WORKSPACE": os.getenv("WORKSPACE"),
            "REPO_SLUG": BITBUCKET_REPO_SLUG,
            "CACHES": ArrayVariable.from_list("CACHES", ['pip', 'docker'])

        })
        self.assertIn('Finished clearing caches', result)
