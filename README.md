# Bitbucket Pipelines pipe: Pipelines clear cache

This pipe lets you clear all or selected Bitbucket Pipelines caches from your repository.

By default, this will clear all dependency caches that exist for this repository, but it can also be configured to clear specific caches.
The pipe will warn you if no caches were found or if it fails to clear a specific cache.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
script:
  - pipe: atlassian/bitbucket-clear-cache:3.6.2
    variables:
      # BITBUCKET_USERNAME: '<string>' # Optional
      # BITBUCKET_APP_PASSWORD: '<string>' # Optional
      # BITBUCKET_ACCESS_TOKEN: '<string>' # Optional
      # CACHES: "<array>"  # Optional
      # DEBUG: "<boolean>" # Optional
      # WORKSPACE: "<string>" # Optional
      # REPO_SLUG: "<string>" # Optional
      # DEBUG: "<string>" # Optional
```
## Variables

| Variable                   | Usage                                                                                                                                      |
|----------------------------|--------------------------------------------------------------------------------------------------------------------------------------------|
| BITBUCKET_USERNAME (1)     | Username of Bitbucket user that can retrieve the repository's caches. Required unless `BITBUCKET_ACCESS_TOKEN` is used.                    |
| BITBUCKET_APP_PASSWORD (1) | Bitbucket app password for the user. Used with `BITBUCKET_USERNAME`. Required unless `BITBUCKET_ACCESS_TOKEN` is used.                     |
| BITBUCKET_ACCESS_TOKEN (1) | The [access token][Bitbucket access token] for the repository. Required unless `BITBUCKET_USERNAME` and `BITBUCKET_APP_PASSWORD` are used. |
| CACHES                     | An optional list of names of caches to clear. Default: `[]`. If empty, will clear all caches.                                              |
| WORKSPACE                  | Workspace of repository to clear cache from. Default: current workspace.                                                                   |
| REPO_SLUG                  | Repository to clear cache from. Default: current repo.                                                                                     |
| DEBUG                      | Turn on extra debug information. Default: `false`.                                                                                         |

_(1) = required variable. Required one of the multiple options._

## Prerequisites

To use this pipe, you need to either [generate an app password][Bitbucket app password] or an [access token][Bitbucket access token].
Remember to check the `Pipelines Write` and `Repositories Read` permissions when generating it.


## Examples

Basic example. This pipe will clear all caches in the repository.

```yaml
script:
  - pipe: atlassian/bitbucket-clear-cache:3.6.2
    variables:
      BITBUCKET_USERNAME: $BITBUCKET_USERNAME
      BITBUCKET_APP_PASSWORD: $BITBUCKET_APP_PASSWORD
```

Basic example. This pipe will clear all caches in the repository. Authentication method is by `BITBUCKET_ACCESS_TOKEN`.

```yaml
script:
  - pipe: atlassian/bitbucket-clear-cache:3.6.2
    variables:
      BITBUCKET_ACCESS_TOKEN: $BITBUCKET_ACCESS_TOKEN
```

Advanced example. This pipe will clear the `maven` and `node` caches in the repository.

```yaml
script:
  - pipe: atlassian/bitbucket-clear-cache:3.6.2
    variables:
      BITBUCKET_USERNAME: $BITBUCKET_USERNAME
      BITBUCKET_APP_PASSWORD: $BITBUCKET_APP_PASSWORD
      CACHES: ["maven", "node"]
```

Advanced example. This pipe will clear caches in the given repository and workspace.

```yaml
script:
  - pipe: atlassian/bitbucket-clear-cache:3.6.2
    variables:
      BITBUCKET_USERNAME: $BITBUCKET_USERNAME
      BITBUCKET_APP_PASSWORD: $BITBUCKET_APP_PASSWORD
      WORKSPACE: "workspace"
      REPO_SLUG: "repo"
```

## Support
If you’d like help with this pipe, or you have an issue or feature request, [let us know on Community][community].

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce


## License
Copyright (c) 2019 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.


[community]: https://community.atlassian.com/t5/forums/postpage/board-id/bitbucket-questions?add-tags=pipes,cache,bitbucket-pipelines
[Bitbucket app password]: https://confluence.atlassian.com/bitbucket/app-passwords-828781300.html
[Bitbucket access token]: https://support.atlassian.com/bitbucket-cloud/docs/access-tokens/
